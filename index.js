import React from 'react';
import { render } from 'react-dom';
import { Provider } from "react-redux";
import { PersistGate } from 'redux-persist/integration/react'

import configureStore from "./store/configureStore";
import Root from './containers/Root';

let { store, persistor } = configureStore();

render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <Root store={store}/>
        </PersistGate>
    </Provider>,
    document.getElementById("app")
);