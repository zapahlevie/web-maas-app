const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    mode: 'production',
    node: {
        console: false,
        net: 'empty',
        dns: 'empty'
    }
});