# MAAS System

Web-based MAAS System App

## Installing depedencies

Run `npm install` to execute download all required depedencies for the project. The depedencies are listed in `package.json` file.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `assets/` directory. Use the `--prod` flag for a production build.
