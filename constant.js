let env = process.env.NODE_ENV || 'production';
let config = {};

if (env == 'production') {
  config = {
    appUrl: 'http://localhost:3333',
    apiUrl: 'https://demo9127840.mockable.io'
  }
} else {
  config = {
    appUrl: 'http://localhost:3333',
    apiUrl: 'https://demo9127840.mockable.io'
  }
}

module.exports = config;
