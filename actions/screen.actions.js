import axios from 'axios';

import constant from '../constant';

export const screenActions = {
    doGetAll, doCreate, doUpdate, doDelete, resetState
};

export const screenConstants = {
    SCREEN_GET_ALL_SUCCESS: 'SCREEN_GET_ALL_SUCCESS',
    SCREEN_GET_ALL_ERROR: 'SCREEN_GET_ALL_ERROR',
    SCREEN_CREATE_SUCCESS: 'SCREEN_CREATE_SUCCESS',
    SCREEN_CREATE_ERROR: 'SCREEN_CREATE_ERROR',
    SCREEN_UPDATE_SUCCESS: 'SCREEN_UPDATE_SUCCESS',
    SCREEN_UPDATE_ERROR: 'SCREEN_UPDATE_ERROR',
    SCREEN_DELETE_SUCCESS: 'SCREEN_DELETE_SUCCESS',
    SCREEN_DELETE_ERROR: 'SCREEN_DELETE_ERROR',
    SCREEN_RESET_STATE: 'SCREEN_RESET_STATE'
}

function doGetAll({ method, url, data }) {
    return dispatch => {
        const config = {
            method: method ? method : 'get',
            url: constant.apiUrl + url,
            data: {
                session_login: localStorage.getItem('token'),
                ...data
            },
            headers: {
                'Content-Type': 'application/json',
            }
        }
        return axios(config)
            .then(response => {
                dispatch({ type: screenConstants.SCREEN_GET_ALL_SUCCESS, data: response.data });
                console.log('call success', response);
            })
            .catch(error => {
                dispatch({ type: screenConstants.SCREEN_GET_ALL_ERROR, errorMsg: error.message });
                console.log('call failed', error.message);
            });
    }

}

function doCreate({ method, url, data }) {
    return dispatch => {
        const config = {
            method: method ? method : 'post',
            url: constant.apiUrl + url,
            data: {
                session_login: localStorage.getItem('token'),
                ...data
            },
            headers: {
                'Content-Type': 'application/json',
            }
        }
        return axios(config)
            .then(response => {
                dispatch({ type: screenConstants.SCREEN_CREATE_SUCCESS });
                console.log('call success', response);
            })
            .catch(error => {
                dispatch({ type: screenConstants.SCREEN_CREATE_ERROR, errorMsg: error.message });
                console.log('call failed', error.message);
            });
    }

}

function doUpdate({ method, url, data }) {
    return dispatch => {
        const config = {
            method: method ? method : 'put',
            url: constant.apiUrl + url,
            data: {
                session_login: localStorage.getItem('token'),
                ...data
            },
            headers: {
                'Content-Type': 'application/json',
            }
        }
        return axios(config)
            .then(response => {
                dispatch({ type: screenConstants.SCREEN_UPDATE_SUCCESS });
                console.log('call success', response);
            })
            .catch(error => {
                dispatch({ type: screenConstants.SCREEN_UPDATE_ERROR, errorMsg: error.message });
                console.log('call failed', error.message);
            });
    }

}

function doDelete({ method, url, data }) {
    return dispatch => {
        const config = {
            method: method ? method : 'delete',
            url: constant.apiUrl + url,
            data: {
                session_login: localStorage.getItem('token'),
                ...data
            },
            headers: {
                'Content-Type': 'application/json',
            }
        }
        return axios(config)
            .then(response => {
                dispatch({ type: screenConstants.SCREEN_DELETE_SUCCESS });
                console.log('call success', response);
            })
            .catch(error => {
                dispatch({ type: screenConstants.SCREEN_DELETE_ERROR, errorMsg: error.message });
                console.log('call failed', error.message);
            });
    }

}

function resetState() {
    return dispatch => {
        dispatch({ type: screenConstants.SCREEN_RESET_STATE });
    }
}