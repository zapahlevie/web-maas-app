import axios from 'axios';

import constant from '../constant';

export const authenticationActions = {
    login, changeActiveMenu, logout
};

export const authenticationConstants = {
    USER_LOGIN_SUCCESS: 'USER_LOGIN_SUCCESS',
    USER_LOGIN_FAILURE: 'USER_LOGIN_FAILURE',
    USER_LOGOUT: 'USER_LOGOUT',
    USER_CHANGE_ACTIVE_MENU: 'USER_CHANGE_ACTIVE_MENU'
}

function login({ user_id, password }) {
    return dispatch => {
        const config = {
            method: 'post',
            url: constant.apiUrl + '/login',
            data: {
                user_id,
                password
            },
            headers: {
                'Content-Type': 'application/json',
            }
        }
        return axios(config)
            .then(response => {
                if (response.status === 200) {
                    localStorage.setItem('user', JSON.stringify(response.data));
                    localStorage.setItem('token', JSON.stringify(response.data.session_login));
                    dispatch({ type: authenticationConstants.USER_LOGIN_SUCCESS, data: response.data });
                    console.log('login success', response);
                }
                else {
                    dispatch({ type: authenticationConstants.USER_LOGIN_FAILURE });
                    console.log('login failed', response);
                }
            })
            .catch(error => {
                dispatch({ type: authenticationConstants.USER_LOGIN_FAILURE });
                console.log('login failed', error);
            });
    }
}

function changeActiveMenu(menu) {
    return dispatch => {
        dispatch({ type: authenticationConstants.USER_CHANGE_ACTIVE_MENU, menu: menu });
    }
}

function logout() {
    return dispatch => {
        dispatch({ type: authenticationConstants.USER_LOGOUT });
        localStorage.removeItem('user');
        localStorage.removeItem('token');
    }
}