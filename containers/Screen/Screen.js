import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, Col, Row, CardBody, Label, Input, FormGroup, FormFeedback, Button, Table, Pagination, PaginationItem, PaginationLink, Nav, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import BarLoader from 'react-spinners/BarLoader';

import { screenActions } from '../../actions/screen.actions';
import { t } from '../../helpers/Translate';
import { validate } from '../../helpers/Validator';
import { exportCSVFile } from '../../helpers/ExportCSV';
import ModalConfirm from '../../Components/ModalConfirm'
import ModalForm from '../../Components/ModalForm'

class Screen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      errorMessage: '',
      successMessage: '',
      searchFieldsValues: {},
      searchValidation: {},
      searchValidationError: {},
      searchResult: [],
      pageNumber: 1,
      pageSize: 10,
      orderBy: '',
      total: 0,
      orderType: 'ASC',
      detailIndex: null,
      shouldDisplayModalDelete: false,
      shouldDisplayModalCreate: false,
      shouldDisplayModalUpdate: false,
      selectedItem: null
    }
    this.handleSearchValueChange = this.handleSearchValueChange.bind(this);
    this.handleResetSearchValue = this.handleResetSearchValue.bind(this);
    this.handleDoSearch = this.handleDoSearch.bind(this);
    this.handleDoDelete = this.handleDoDelete.bind(this);
    this.handleDoCreate = this.handleDoCreate.bind(this);
    this.handleDoUpdate = this.handleDoUpdate.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleSortBy = this.handleSortBy.bind(this);
    this.handlePaging = this.handlePaging.bind(this);
    this.handleDetailIndex = this.handleDetailIndex.bind(this);
    this.setModalVisibility = this.setModalVisibility.bind(this);
    this.exportObject = this.exportObject.bind(this);
  }

  componentDidMount() {
    const { dispatch, config } = this.props;

    let searchFieldsValues = {};
    let searchValidation = {};
    let searchValidationError = {};

    config.searchFields && config.searchFields.map(field => {

      //search field values
      if (field.type === 'group') {
        searchFieldsValues[field.name] = field.fields[0] ? field.fields[0].name : '';
        field.fields.map(groupField => {
          searchFieldsValues[groupField.name] = groupField.defaultValue ? groupField.defaultValue : '';
        })
      }
      else if (field.type === 'date-range') {
        searchFieldsValues[field.fields[0].name] = field.fields[0].defaultValue ? field.fields[0].defaultValue : '';
        searchFieldsValues[field.fields[1].name] = field.fields[1].defaultValue ? field.fields[1].defaultValue : '';
      }
      else if (field.type === 'checkbox') {
        searchFieldsValues[field.name] = (field.defaultValue && field.defaultValue.length > 0) ? [...field.defaultValue] : [];
      }
      else {
        searchFieldsValues[field.name] = field.defaultValue ? field.defaultValue : '';
      }

      //search validation
      if (field.type === 'group') {
        field.fields.map(groupField => {
          if (groupField.validation && groupField.validation.length > 0) {
            searchValidation[groupField.name] = groupField.validation;
            searchValidationError[groupField.name] = false;
          }
        });
      }
      else if (field.type === 'date-range') {
        searchValidation[field.fields[0].name] = field.fields[0].validation;
        searchValidation[field.fields[1].name] = field.fields[0].validation;
        searchValidationError[field.fields[0].name] = false;
        searchValidationError[field.fields[0].name] = false;
      }
      else {
        if (field.validation && field.validation.length > 0) {
          searchValidation[field.name] = field.validation;
          searchValidationError[field.name] = false;
        }
      }

    })

    dispatch(screenActions.resetState());
    this.setState({ searchFieldsValues, searchValidation, searchValidationError });
    console.log('screen props', this.props);
    console.log('screen reducer', this.props.screen);

  }

  componentDidUpdate() {

  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(screenActions.resetState());
    console.log('execute willunmount...');
  }

  setModalVisibility(key, v) {
    switch (key) {
      case 'create':
        this.setState({ shouldDisplayModalCreate: v });
        break;
      case 'update':
        this.setState({ shouldDisplayModalUpdate: v });
        break;
      case 'delete':
        this.setState({ shouldDisplayModalDelete: v });
        break;
    }
  }

  handleSearchValueChange(key, value) {

    if (Array.isArray(this.state.searchFieldsValues[key])) {
      let arrValues = [...this.state.searchFieldsValues[key]];
      if (arrValues.includes(value)) {
        arrValues = arrValues.filter(val => { return val != value; })
      }
      else {
        arrValues.push(value);
      }
      this.setState({ searchFieldsValues: Object.assign({}, this.state.searchFieldsValues, { [key]: arrValues }) });
    }
    else {
      this.setState({ searchFieldsValues: Object.assign({}, this.state.searchFieldsValues, { [key]: value }) });
    }

  }

  handleResetSearchValue() {
    let searchFieldsValues = {};
    Object.keys(this.state.searchFieldsValues).map(key => {
      if (Array.isArray(this.state.searchFieldsValues[key])) {
        searchFieldsValues[key] = [];
      }
      else {
        searchFieldsValues[key] = '';
      }
    })
    this.setState({ searchFieldsValues: searchFieldsValues });
  }

  handlePaging(key, value) {
    this.setState({ key, value }, () => {
      this.handleDoSearch();
    });
  }

  handleSortBy(key) {
    if (!this.state.isLoading) {
      if (this.state.orderBy === key) {
        if (this.state.orderType === 'ASC') {
          this.setState({ orderType: 'DESC' }, () => {
            this.handleDoSearch();
          })
        }
        else {
          this.setState({ orderBy: '' }, () => {
            this.handleDoSearch();
          })
        }
      }
      else {
        this.setState({ orderBy: key, orderType: 'ASC' }, () => {
          this.handleDoSearch();
        })
      }
    }
  }

  handleDoSearch(isStateReload = true, getAllPages = false) {
    return new Promise((resolve, reject) => {
      const { dispatch, config } = this.props;

      if (this.isSearchFieldsValidationPassed()) {
        let method = config.searchOptions.apiConfig.method;
        let url = config.searchOptions.apiConfig.url;
        let data = { ...config.searchOptions.apiConfig.data };

        Object.keys(data).map(key => {
          if (getAllPages && data[key] === 'pageNumber') {
            data[key] = 1;
          }
          else if (getAllPages && data[key] === 'pageSize') {
            data[key] = this.state.total;
          }
          else {
            data[key] = this.state.searchFieldsValues[data[key]] || this.state[data[key]];
          }
        })

        url.match(/[^{}]*(?=\})/g) && url.match(/[^{}]*(?=\})/g).map(param => {
          if (getAllPages && param === 'pageNumber') {
            url = url.replace(new RegExp('{' + param + '}', 'gi'), 1);
          }
          else if (getAllPages && param === 'pageSize') {
            url = url.replace(new RegExp('{' + param + '}', 'gi'), this.state.total);
          }
          else {
            url = url.replace(new RegExp('{' + param + '}', 'gi'), this.state.searchFieldsValues[param] || this.state[param]);
          }
        })

        this.setState({ isLoading: true, errorMessage: '', successMessage: '' });
        dispatch(screenActions.doGetAll(
          {
            method: method,
            url: url,
            data: data
          }
        )).then(() => {
          this.setState({ isLoading: false, detailIndex: null });
          if (this.props.screen.error) {
            this.setState({ errorMessage: this.props.screen.error });
          }
          if (isStateReload) {
            this.setState({ searchResult: this.props.screen.getAllResult, total: this.props.screen.getAllTotal });
          }
          console.log('screen props', this.props);
          console.log('screen reducer', this.props.screen);
          resolve(this.props.screen.getAllResult);
        })
      }
    });

  }

  handleDelete(item, index) {
    this.setState({ selectedItem: { item, index } }, () => {
      this.setModalVisibility('delete', true);
    })
  }

  handleDoDelete() {
    const { dispatch, config } = this.props;

    if (this.state.selectedItem) {
      const { item, index } = this.state.selectedItem

      let method = config.deleteOptions.apiConfig.method;
      let url = config.deleteOptions.apiConfig.url;
      let data = { ...config.deleteOptions.apiConfig.data };

      Object.keys(data).map(key => {
        data[key] = item[key];
      })

      url.match(/[^{}]*(?=\})/g) && url.match(/[^{}]*(?=\})/g).map(param => {
        url = url.replace(new RegExp('{' + param + '}', 'gi'), item[param]);
      })

      this.setState({ isLoading: true, errorMessage: '', successMessage: '', shouldDisplayModalDelete: false });
      dispatch(screenActions.doDelete(
        {
          method: method,
          url: url,
          data: data
        }
      )).then(() => {
        this.setState({ isLoading: false });
        if (this.props.screen.error) {
          this.setState({ errorMessage: this.props.screen.error });
        }
        else {
          this.setState({ successMessage: t('Data successfully deleted'), detailIndex: null });
          //TO DO REMOVE
          this.setState({ searchResult: this.state.searchResult.filter((data, i) => { return i !== index }) });
          // this.handleDoSearch();
        }
        console.log('screen props', this.props);
        console.log('screen reducer', this.props.screen);
      })
    }

  }

  handleCreate() {
    this.setModalVisibility('create', true);
  }

  handleDoCreate(item) {
    const { dispatch, config } = this.props;

    let method = config.createOptions.apiConfig.method;
    let url = config.createOptions.apiConfig.url;
    let data = { ...config.createOptions.apiConfig.data };

    Object.keys(data).map(key => {
      data[key] = item[key];
    })

    url.match(/[^{}]*(?=\})/g) && url.match(/[^{}]*(?=\})/g).map(param => {
      url = url.replace(new RegExp('{' + param + '}', 'gi'), item[param]);
    })

    this.setState({ isLoading: true, errorMessage: '', successMessage: '', shouldDisplayModalCreate: false });
    dispatch(screenActions.doCreate(
      {
        method: method,
        url: url,
        data: data
      }
    )).then(() => {
      this.setState({ isLoading: false });
      if (this.props.screen.error) {
        this.setState({ errorMessage: this.props.screen.error });
      }
      else {
        this.setState({ successMessage: t('Data successfully added'), detailIndex: null });
        //TO DO REMOVE
        this.setState({ searchResult: [...this.state.searchResult, item] });
        // this.handleDoSearch();
      }
      console.log('screen props', this.props);
      console.log('screen reducer', this.props.screen);
    })

  }

  handleUpdate(item, index) {
    this.setState({ selectedItem: { item, index } }, () => {
      this.setModalVisibility('update', true);
    })
  }

  handleDoUpdate(item) {
    const { dispatch, config } = this.props;

    const { index } = this.state.selectedItem

    let method = config.updateOptions.apiConfig.method;
    let url = config.updateOptions.apiConfig.url;
    let data = { ...config.updateOptions.apiConfig.data };

    Object.keys(data).map(key => {
      data[key] = item[key];
    })

    url.match(/[^{}]*(?=\})/g) && url.match(/[^{}]*(?=\})/g).map(param => {
      url = url.replace(new RegExp('{' + param + '}', 'gi'), item[param]);
    })

    this.setState({ isLoading: true, errorMessage: '', successMessage: '', shouldDisplayModalUpdate: false });
    dispatch(screenActions.doUpdate(
      {
        method: method,
        url: url,
        data: data
      }
    )).then(() => {
      this.setState({ isLoading: false });
      if (this.props.screen.error) {
        this.setState({ errorMessage: this.props.screen.error });
      }
      else {
        this.setState({ successMessage: t('Data successfully updated'), detailIndex: null });
        //TO DO REMOVE
        this.setState({ searchResult: Object.assign([], this.state.searchResult, { [index]: Object.assign({}, this.state.searchResult[index], item) }) });
        // this.handleDoSearch();
      }
      console.log('screen props', this.props);
      console.log('screen reducer', this.props.screen);
    })

  }

  isSearchFieldsValidationPassed() {
    //Clear Error
    let searchValidationError = {};
    Object.keys(this.state.searchValidationError).map(key => {
      searchValidationError[key] = false;
    })
    this.setState({ searchValidationError });

    //do validate
    Object.keys(this.state.searchValidation).map(key => {

      let value = this.state.searchFieldsValues[key];
      let validators = this.state.searchValidation[key];

      if (!validate(value, validators)) {
        searchValidationError[key] = true;
      }

    })

    //check if error exist
    let isValidationPassed = true;
    Object.keys(this.state.searchValidationError).map(key => {
      if (searchValidationError[key]) {
        isValidationPassed = false;
      }
    })

    return isValidationPassed;
  }

  printValidation(validators) {
    if (Array.isArray(validators)) {
      let arr = [];
      validators.map((value, index) => {
        if (value.indexOf('minlength-') === 0) {
          arr.push(t('at least [n] characters', { n: value.split('-')[1] }));
        }
        else if (value.indexOf('maxlength-') === 0) {
          arr.push(t('[n] characters maximum', { n: value.split('-')[1] }));
        }
        else {
          arr.push(t(value));
        }
      })
      return arr.toString().split(',').join(', ');
    }
    else {
      return '';
    }
  }

  handleDetailIndex(value) {
    if (value === this.state.detailIndex) {
      this.setState({ detailIndex: null });
    }
    else {
      this.setState({ detailIndex: value });
    }
  }

  exportObject() {
    const { dispatch, config } = this.props;
    const { searchResult, total } = this.state;

    let filename = config.code + "_" + config.name + "_" + Date.now();

    if (config.searchOptions.disablePaging) {
      exportCSVFile(searchResult, filename);
    }
    else {
      if (total > 0) {
        this.handleDoSearch(false, true).then(response => {
          exportCSVFile(response, filename);
        });
      }
    }
  }

  render() {

    const { config } = this.props;
    const { searchResult, searchFieldsValues, isLoading, searchValidationError, searchValidation, pageSize, pageNumber, total } = this.state;

    let pageNumbers = [];
    for (let i = (pageNumber - 3) > 0 ? (pageNumber - 3) : 1; i <= Math.ceil(total / pageSize) && i <= pageNumber + 3; i++) {
      pageNumbers.push(i);
    }

    let updateFields = config.updateFields.map(field => {
      if (!field.defaultValue && this.state.selectedItem) {
        field.defaultValue = this.state.selectedItem.item[field.name];
      }
      return field;
    })


    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardBody>
                <div className="mb-3"><span className="bg-primary p-1">{config.code}</span>&nbsp;<h5 className="u-inline">{t(config.name)}</h5></div>

                {this.state.errorMessage && <div className="alert alert-danger">{this.state.errorMessage}</div>}
                {this.state.successMessage && <div className="alert alert-success">{this.state.successMessage}</div>}

                {/* FORM SEARCH */}
                {!config.disableSearch && <>
                  {config.searchFields && config.searchFields.map((field, index) => {

                    if (field.type === 'date-range') {
                      return (
                        <FormGroup row key={index}>
                          <Col xs="3" sm="3" md="3">
                            <Label htmlFor={field.fields[0].name}>{t(field.label)}</Label>
                          </Col>
                          <Col xs="4" sm="4" md="4">
                            <Input invalid={searchValidationError[field.fields[0].name]} onChange={(e) => this.handleSearchValueChange(field.fields[0].name, e.target.value)} name={field.fields[0].name} type="date" value={searchFieldsValues[field.fields[0].name] || ''} />
                            <FormFeedback>{t('This field is')} {this.printValidation(searchValidation[field.name])}</FormFeedback>
                          </Col>
                          <Col xs="1" sm="1" md="1" className="text-center">
                            <Label htmlFor={field.fields[1].name}>-</Label>
                          </Col>
                          <Col xs="4" sm="4" md="4">
                            <Input invalid={searchValidationError[field.fields[1].name]} onChange={(e) => this.handleSearchValueChange(field.fields[1].name, e.target.value)} name={field.fields[1].name} type="date" value={searchFieldsValues[field.fields[1].name] || ''} />
                            <FormFeedback>{t('This field is')} {this.printValidation(searchValidation[field.name])}</FormFeedback>
                          </Col>
                        </FormGroup>
                      )
                    }
                    else if (field.type === 'date') {
                      return (
                        <FormGroup row key={index}>
                          <Col xs="3" sm="3" md="3">
                            <Label htmlFor={field.name}>{t(field.label)}</Label>
                          </Col>
                          <Col xs="9" sm="9" md="9">
                            <Input invalid={searchValidationError[field.name]} onChange={(e) => this.handleSearchValueChange(field.name, e.target.value)} name={field.name} type="date" value={searchFieldsValues[field.name] || ''} />
                            <FormFeedback>{t('This field is')} {this.printValidation(searchValidation[field.name])}</FormFeedback>
                          </Col>
                        </FormGroup>
                      )
                    }
                    else if (field.type === 'group') {
                      return (
                        <FormGroup row key={index}>
                          <Col xs="3" sm="3" md="3">
                            <Input onChange={(e) => this.handleSearchValueChange(field.name, e.target.value)} type="select" name={field.name} value={searchFieldsValues[field.name] || ''}>
                              {field.fields.map((groupField, index) => {
                                return <option key={index} value={groupField.name}>{t(groupField.label)}</option>
                              })}
                            </Input>
                          </Col>
                          <Col xs="9" sm="9" md="9">
                            {field.fields.map((groupField, index) => {
                              let activeGroupField = searchFieldsValues[field.name] || '';
                              let isActive = false;
                              if (activeGroupField) {
                                isActive = activeGroupField === groupField.name;
                              }
                              else {
                                if (index === 0) {
                                  isActive = true;
                                }
                              }
                              return (
                                <div key={index} className={isActive ? '' : 'd-none'}>
                                  <Input invalid={searchValidationError[groupField.name]} onChange={(e) => this.handleSearchValueChange(groupField.name, e.target.value)} name={groupField.name} type={groupField.type ? groupField.type : 'text'} value={searchFieldsValues[groupField.name] || ''} />
                                  <FormFeedback>{t('This field is')} {this.printValidation(searchValidation[groupField.name])}</FormFeedback>
                                </div>
                              )
                            })}
                          </Col>
                        </FormGroup>
                      )
                    }
                    else if (field.type === 'select') {
                      return (
                        <FormGroup row key={index}>
                          <Col xs="3" sm="3" md="3">
                            <Label htmlFor={field.name}>{t(field.label)}</Label>
                          </Col>
                          <Col xs="9" sm="9" md="9">
                            <Input invalid={searchValidationError[field.name]} onChange={(e) => this.handleSearchValueChange(field.name, e.target.value)} name={field.name} type="select" value={searchFieldsValues[field.name] || ''} >
                              <option value="">{t('Please select')}...</option>
                              {field.options.map((option, index) => {
                                return <option key={index} value={option.value}>{option.label}</option>
                              })}
                            </Input>
                            <FormFeedback>{t('This field is')} {this.printValidation(searchValidation[field.name])}</FormFeedback>
                          </Col>
                        </FormGroup>
                      )
                    }
                    else if (field.type === 'radio') {
                      return (
                        <FormGroup row key={index}>
                          <Col xs="3" sm="3" md="3">
                            <Label htmlFor={field.name}>{t(field.label)}</Label>
                          </Col>
                          <Col xs="9" sm="9" md="9">
                            {field.options.map((option, index) => {
                              let isChecked = searchFieldsValues[field.name] && (searchFieldsValues[field.name] === option.value);
                              return (
                                <Label key={index} htmlFor={field.name} className="ml-4 mr-2">
                                  <Input invalid={searchValidationError[field.name]} onChange={(e) => this.handleSearchValueChange(field.name, e.target.value)} name={field.name} type="radio" value={option.value} checked={isChecked ? true : false} />
                                  {option.label}
                                  <FormFeedback>{t('This field is')} {this.printValidation(searchValidation[field.name])}</FormFeedback>
                                </Label>
                              )
                            })}
                          </Col>
                        </FormGroup>
                      )
                    }
                    else if (field.type === 'checkbox') {
                      return (
                        <FormGroup row key={index}>
                          <Col xs="3" sm="3" md="3">
                            <Label htmlFor={field.name}>{t(field.label)}</Label>
                          </Col>
                          <Col xs="9" sm="9" md="9">
                            {field.options.map((option, index) => {
                              let isChecked = searchFieldsValues[field.name] && (searchFieldsValues[field.name].length > 0) && searchFieldsValues[field.name].includes(option.value);
                              return (
                                <Label key={index} htmlFor={field.name} className="ml-4 mr-2">
                                  <Input invalid={searchValidationError[field.name]} onChange={(e) => this.handleSearchValueChange(field.name, e.target.value)} name={field.name} type="checkbox" value={option.value} checked={isChecked ? true : false} />
                                  {option.label}
                                  <FormFeedback>{t('This field is')} {this.printValidation(searchValidation[field.name])}</FormFeedback>
                                </Label>
                              )
                            })}
                          </Col>
                        </FormGroup>
                      )
                    }
                    else {
                      return (
                        <FormGroup row key={index}>
                          <Col xs="3" sm="3" md="3">
                            <Label htmlFor={field.name}>{t(field.label)}</Label>
                          </Col>
                          <Col xs="9" sm="9" md="9">
                            <Input invalid={searchValidationError[field.name]} onChange={(e) => this.handleSearchValueChange(field.name, e.target.value)} name={field.name} type={field.type ? field.type : 'text'} value={searchFieldsValues[field.name] || ''} />
                            <FormFeedback>{t('This field is')} {this.printValidation(searchValidation[field.name])}</FormFeedback>
                          </Col>
                        </FormGroup>
                      )
                    }
                  })}

                  <FormGroup row>
                    <Col>
                      {!config.searchOptions.hideButtonReset && <Button onClick={this.handleResetSearchValue} className="pull-right px-4 ml-2" color="light"><i className="fa fa-refresh">{' ' + t('Reset')}</i></Button>}
                      <Button onClick={this.handleDoSearch} className="pull-right px-4" color="primary" disabled={isLoading}><i className="fa fa-search">{' ' + t('Search')}</i></Button>
                    </Col>
                  </FormGroup>

                  <Row className="mt-4">
                    <Col xs="6" sm="6">
                      {!config.searchOptions.disablePaging && <>
                        <span className="inline-block">{t('Items per page')}</span>
                        <Input type="select" className="inline-block mx-2" onChange={(e) => this.handlePaging('PageNumber', e.target.value)} style={{ width: "auto" }}>
                          <option value="10">10</option>
                          <option value="25">25</option>
                          <option value="50">50</option>
                          <option value="100">100</option>
                        </Input>
                      </>}
                    </Col>
                    <Col xs="6" sm="6">
                      {!config.disableExport &&
                        <Nav navbar className="pull-right" >
                          <UncontrolledDropdown nav direction="down">
                            <DropdownToggle nav className="p-0 ml-2">
                              <Button color="primary"><i className="fa fa-download"></i></Button>
                            </DropdownToggle>
                            <DropdownMenu right>
                              <DropdownItem onClick={() => this.exportObject()}>{t('Export to CSV')}</DropdownItem>
                            </DropdownMenu>
                          </UncontrolledDropdown>
                        </Nav>
                      }
                      {!config.disableCreate &&
                        <Button onClick={() => this.handleCreate()} className="pull-right" color="primary"><i className="fa fa-plus"></i></Button>
                      }
                    </Col>
                  </Row>

                  {/* SEARCH TABLE RESULT */}
                  <Table responsive className="text-nowrap mt-2 table-strip">
                    <thead>
                      <tr>
                        <th>#</th>
                        {Object.keys(config.searchTable.column || []).map((key, index) => {
                          if (config.searchOptions.disableSorting) {
                            return <th key={index}>{t(key)}</th>
                          }
                          else {
                            return <th key={index} className={"u-cursor-pointer noselect " + (this.state.orderBy === key ? 'text-secondary' : '')} onClick={() => this.handleSortBy(key)}>{t(key)}&nbsp;
                            {this.state.orderBy !== key && <i className="fa fa-sort"></i>}
                              {this.state.orderBy === key && this.state.orderType === 'ASC' && <i className="fa fa-sort-asc"></i>}
                              {this.state.orderBy === key && this.state.orderType === 'DESC' && <i className="fa fa-sort-desc"></i>}
                            </th>
                          }
                        })}
                      </tr>
                      <tr>
                        <th colSpan={Object.keys(config.searchTable.column || []).length + 1} className="p-0 m-0">
                          <BarLoader width="auto" loading={isLoading} />
                        </th>
                      </tr>
                    </thead>
                    {searchResult.map((result, index) => {
                      let orderNumber = 0;
                      let isDetailOpen = false;

                      if (!config.searchOptions.disablePaging) {
                        orderNumber = ((pageNumber - 1) * pageSize) + (index + 1);
                      }
                      else {
                        orderNumber = index + 1;
                      }
                      if (this.state.detailIndex === index) {
                        isDetailOpen = true;
                      }
                      return (<tbody key={index}>
                        <tr className="tr-hover" onClick={() => this.handleDetailIndex(index)}>
                          <td>{orderNumber}</td>
                          {Object.values(config.searchTable.column || []).map((value, indexValue) => {
                            return <td key={indexValue}>{result[value]}</td>
                          })}
                        </tr>

                        <tr className={isDetailOpen ? "tr-detail u-text-small" : "tr-detail hidden"}>
                          <td className={isDetailOpen ? "" : "p-0"} colSpan={Object.keys(config.searchTable.column || []).length + 1}>
                            <div className={isDetailOpen ? "" : "d-none"}>
                              {!config.disableDetail && Object.keys(config.actionDetail.column || []).map((key, indexKey) => {
                                return <div key={indexKey}><strong>{t(key)}</strong>:&nbsp;{result[config.actionDetail.column[key]]}</div>
                              })}
                              {!config.disableEdit &&
                                <Button onClick={() => this.handleUpdate(result, index)} className="u-text-small mt-2 mr-2" color="primary">{t('Update')}</Button>
                              }
                              {!config.disableDelete &&
                                <Button onClick={() => this.handleDelete(result, index)} className="u-text-small mt-2" color="danger">{t('Delete')}</Button>
                              }
                            </div>
                          </td>
                        </tr>

                      </tbody>)
                    })}
                  </Table>

                  {!config.searchOptions.disablePaging && <>
                    <Row className="mt-2">
                      <Col md="">
                        <Pagination className="pull-right">
                          <PaginationItem disabled={total ? false : true}><PaginationLink first={true} onClick={() => { this.handlePaging('pageNumber', 1) }} tag="button" /></PaginationItem>
                          {(total > 0) && pageNumbers.map(number => {
                            return (
                              <PaginationItem active={(pageNumber === number) ? true : false} key={number}>
                                <PaginationLink onClick={() => { this.handlePaging('pageNumber', number) }} tag="button">{number}</PaginationLink>
                              </PaginationItem>
                            );
                          })}
                          <PaginationItem disabled={total ? false : true}><PaginationLink last={true} onClick={() => { this.handlePaging('pageNumber', Math.ceil(total / pageSize)) }} tag="button" /></PaginationItem>
                        </Pagination>
                      </Col>
                    </Row>
                  </>}

                </>}
              </CardBody>
            </Card>
          </Col>
        </Row>

        {this.state.shouldDisplayModalDelete && <ModalConfirm
          toggle={() => this.setModalVisibility('delete', !this.state.shouldDisplayModalDelete)}
          confirm={this.handleDoDelete}
          header="Delete Confirmation"
          body="Are you sure you want to delete this item?"
        />}

        {this.state.shouldDisplayModalCreate && <ModalForm
          toggle={() => this.setModalVisibility('create', !this.state.shouldDisplayModalCreate)}
          confirm={this.handleDoCreate}
          header="Add Data"
          fields={config.createFields}
        />}

        {this.state.shouldDisplayModalUpdate && <ModalForm
          toggle={() => this.setModalVisibility('update', !this.state.shouldDisplayModalUpdate)}
          confirm={this.handleDoUpdate}
          header="Edit Data"
          fields={updateFields}
        />}

      </div>
    );
  }
}

function mapStateToProps(state) {

  return {
    user: state.authentication.user,
    screen: state.screen,
  };
}

export default connect(mapStateToProps)(Screen);
