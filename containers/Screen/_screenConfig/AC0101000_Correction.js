import { format } from 'date-fns';

export default {
    name: 'Correction',
    code: 'AC0101000',
    disableSearch: false, //optional default false
    disableDetail: false, //optional default false
    disableCreate: false, //optional default false
    disableEdit: false, //optional default false
    disableDelete: false, //optional default false
    searchFields: [ //currently supported type: date-range, text, number, date, select, radio, checkbox, group
        {
            type: 'date-range',
            label: 'Posting date',
            fields: [
                {
                    name: 'post_date_from', //required unique no space
                    validation: ['required'],
                    defaultValue: format(new Date(), 'yyyy-MM-dd'),
                },
                {
                    name: 'post_date_to',
                    validation: ['required'],
                    defaultValue: format(new Date(), 'yyyy-MM-dd')
                }
            ]
        },
        {
            type: 'select',
            label: 'Card Type',
            name: 'card_type',
            options: [
                { label: 'Kredit', value: '001' }, { label: 'Debit', value: '002' }
            ]
        },
        {
            type: 'radio',
            label: 'Issuer',
            name: 'issuer',
            defaultValue: '008',
            options: [
                { label: 'Mandiri', value: '008' }, { label: 'Other', value: '000' }
            ]
        },
        {
            type: 'checkbox',
            label: 'Status',
            name: 'status',
            defaultValue: ['all'],
            validation: ['required'],
            options: [
                { label: 'All', value: 'all' }, { label: 'Success', value: 'success' }, { label: 'Failed', value: 'failed' }
            ]
        },
        {
            type: 'group',
            name: 'correctionOptions',
            fields: [ //support standar input text/number/date
                {
                    type: 'text',
                    label: 'MID',
                    name: 'mid',
                    defaultValue: '21',
                    validation: ['required', 'numeric', 'minlength-2'] //currently supported: required
                },
                {
                    type: 'number',
                    label: 'Batch No.',
                    name: 'batch_no'
                }
            ]
        },
    ],
    searchOptions: {
        hideButtonReset: false, //optional default false
        disablePaging: false, //optional default false
        disableSorting: false, //optional default false
        apiConfig: {
            method: 'get', //optional default 'post'
            url: '/getCorrections?page={pageNumber}&pageSize={pageSize}&orderBy={orderBy}&orderType={orderType}&mid={mid}',
            data: {
                post_date_from: 'post_date_from', //taken from search field name
                post_date_to: 'post_date_to',
                mid: 'mid',
                batch_no: 'batch_no',
                issuer: 'issuer'
            },
        }
    },
    searchTable: {
        column: { //taken from api response model 
            "Posting Date": "posting_date",
            "Batch Number": "batch_no",
            "MID": "mid",
            "MCC": "mcc",
            "Auth Date": "auth_date",
            "Card Number": "card_number",
            "Auth No": "auth_no",
            "Posting Amount": "posting_amount",
            "Code": "code",
            "Card Type": "card_type",
            "User Name": "user_name",
            "Memo": "memo",
            "Auth Batch No": "auth_batch_no",
            "Posting Type": "posting_type",
            "Original Amount": "original_amount",
            "Tip": "tip",
            "Tax": "tax",
            "Issuer": "issuer",
        }
    },
    actionDetail: {
        column: { //taken from api response model 
            "Sale Kind": "sale_knd",
            "Posting Date": "posting_date",
            "Batch Number": "batch_no",
            "MID": "mid",
            "MCC": "mcc",
            "Auth Date": "auth_date",
            "Card Number": "card_number",
            "Auth No": "auth_no",
            "Posting Amount": "posting_amount",
            "Ins": "ins",
            "Code": "code",
            "Card Type": "card_type",
            "User Name": "user_name",
            "Memo": "memo",
            "Auth Batch No": "auth_batch_no",
            "Posting Type": "posting_type",
            "Original Amount": "original_amount",
            "Tip": "tip",
            "Tax": "tax",
            "V": "v",
            "Fund Collection": "fund_collection",
            "Collection Amount": "collection_amount",
            "Issuer": "issuer",
            "PWCW Amount": "pwcw_amount",
            "Old MID": "old_mid",
            "Posting Number": "posting_number"
        }
    },
    createFields: [
        {
            type: 'select',
            label: 'Card Type',
            name: 'card_type',
            options: [
                { label: 'Kredit', value: '001' }, { label: 'Debit', value: '002' }
            ]
        },
        {
            type: 'text',
            label: 'MID',
            validation: ['required'],
            name: 'mid',
            defaultValue: '8192'
        },
        {
            type: 'date',
            label: 'Posting Date',
            name: 'posting_date'
        },
    ],
    createOptions: {
        apiConfig: {
            method: 'post', //optional default 'post'
            url: '/addCorrection',
            data: {
                card_type: 'card_type', //taken from create field name
                posting_date: 'posting_date',
                mid: 'mid'
            },
        }
    },
    updateFields: [
        {
            type: 'select',
            label: 'Card Type',
            name: 'card_type',
            options: [
                { label: 'Kredit', value: '001' }, { label: 'Debit', value: '002' }
            ]
        },
        {
            type: 'text',
            label: 'MID',
            validation: ['required'],
            name: 'mid'
        },
        {
            type: 'date',
            label: 'Posting Date',
            name: 'posting_date',
            defaultValue: format(new Date(), 'yyyy-MM-dd')
        },
    ],
    updateOptions: {
        apiConfig: {
            method: 'put', //optional default 'post'
            url: '/updateCorrection',
            data: {
                card_type: 'card_type', //taken from create field name
                posting_date: 'posting_date',
                mid: 'mid'
            },
        }
    },
    deleteOptions: {
        apiConfig: {
            method: 'delete', //optional default 'post'
            url: '/deleteCorrection?mid={mid}'
        }
    },
}