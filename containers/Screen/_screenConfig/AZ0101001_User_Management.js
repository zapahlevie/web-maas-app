export default {
    name: 'User Management',
    code: 'AZ0101001',
    disableSearch: false, //optional default false
    disableDetail: false, //optional default false
    disableCreate: false, //optional default false
    disableEdit: false, //optional default false
    disableDelete: false, //optional default false
    searchFields: [],
    searchOptions: {},
    searchTable: {},
    actionDetail: {},
    createFields: [],
    createOptions: {},
    updateFields: [],
    updateOptions: {},
    deleteOptions: {}
}