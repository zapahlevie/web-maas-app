import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, CardHeader, CardTitle, Col, Row, CardBody } from 'reactstrap';

import { t } from '../helpers/Translate';

class Dashboard extends Component {
  constructor(props) {
    super(props);

  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {
    return (
      <div className="animated fadeIn">

        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardBody>
                <CardTitle><h5>Notice</h5></CardTitle>
                <div className="table-responsive">
                  <table className="text-nowrap table mt-2 table-hover">
                    <thead>
                      <tr>
                        <th>TYPE</th>
                        <th>TITLE</th>
                        <th>CNT</th>
                        <th>REG DATE</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>

              </CardBody>
            </Card>
          </Col>
        </Row>

      </div>
    );
  }
}

function mapStateToProps(state) {

  return {
    user: state.authentication.user,
  };
}

export default connect(mapStateToProps)(Dashboard);
