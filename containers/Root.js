import React from 'react';
import { connect } from 'react-redux';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import Login from './Login';
import DefaultLayout from './DefaultLayout/DefaultLayout';
import '../assets/scss/App.scss';

const history = createBrowserHistory();
const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;
const PrivateRoute = ({ component: Component }) => (
    <Route render={props => {
        if (!localStorage.getItem('user')) {
            return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        }
        return <Component {...props} />
    }} />
)

class Root extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="base">
                <Router history={history}>
                    <React.Suspense fallback={loading()}>
                        <Switch>
                            <Route path="/login" component={Login} />
                            <PrivateRoute path="/" name="Home" component={DefaultLayout} />
                        </Switch>
                    </React.Suspense>
                </Router>
            </div>
        );
    }
}


function mapStateToProps(state) {

    return {
        user: state.authentication.user
    };
}

export default connect(mapStateToProps)(Root);