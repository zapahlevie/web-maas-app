import React from 'react';

import configAC0101000 from '../Screen/_screenConfig/AC0101000_Correction';
import configAZ0101001 from '../Screen/_screenConfig/AZ0101001_User_Management';

const Dashboard = React.lazy(() => import('../Dashboard'));
const Screen = React.lazy(() => import('../Screen/Screen'));

export default [
  { path: '/', exact: true, name: 'Home' },
  { path: '/home', name: 'Dashboard', component: Dashboard },
  { path: '/AC0101000', name: 'Correction', component: Screen, config: configAC0101000 },
  { path: '/AZ0101001', name: 'User Management', component: Screen, config: configAZ0101001 },
];
