import React, { Component } from 'react';
import { connect } from 'react-redux';
import { UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, Button } from 'reactstrap';
import { AppSidebarToggler } from '@coreui/react';
import { Link } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import { authenticationActions } from '../../actions/authentication.actions';
import { t } from '../../helpers/Translate';
import screenList from './_screenList';
import defaultConfig from './_defaultConfig';

class DefaultHeader extends Component {

  constructor(props) {
    super(props);

    this.state = {
      menu: [],
      activeLang: localStorage.getItem('lang') || navigator.language || navigator.userLanguage,
      width: window.innerWidth
    };

    this.logout = this.logout.bind(this);
    this.switchMenu = this.switchMenu.bind(this);
    this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this);

  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  componentDidMount() {
    let menu = [];
    screenList.map(item => {
      if (!item.hide) {
        menu.push(item.name);
      }
    })
    this.setState({ menu: menu });

    window.addEventListener('resize', this.handleWindowSizeChange);
    console.log('Default header props', this.props);
  }

  logout() {
    const { dispatch } = this.props;

    dispatch(authenticationActions.logout());
  }

  saveLanguage(lang) {
    localStorage.setItem('lang', lang);
    window.location.reload(true);
  }

  switchMenu(menu) {
    const { dispatch } = this.props;

    dispatch(authenticationActions.changeActiveMenu(menu));
  }

  render() {
    return (
      <React.Fragment>

        {/* SIDEBAR TOGGLER TABLET */}
        <AppSidebarToggler className="u-outline-0 d-lg-none" display="md" mobile />

        {/* LOGO DESKTOP */}
        <div className="u-hidden-down@tablet h-desktop">
          <Link to="/" className="u-text-decoration-0">
            <div className="text-center">
              <span className="u-text-large text-secondary">MAAS&nbsp;</span>
              <span className="u-text-large text-white">System</span>
            </div>
          </Link>
        </div>

        {/* SIDEBAR TOGGLER DESKTOP */}
        <AppSidebarToggler className="u-outline-0 d-md-down-none" display="lg" />

        {/* LOGO TABLET */}
        <div className="u-hidden-up@tablet h-tablet">
          <Link to="/" className="u-text-decoration-0">
            <div className="text-center">
              <span className="u-text-large text-secondary">MAAS&nbsp;</span>
              <span className="u-text-large text-white">System</span>
            </div>
          </Link>
        </div>

        {/* TOP MENU DESKTOP */}
        {defaultConfig.enableTopMenu && <div className="u-hidden-down@tablet">
          <div className="timeline-tab-parent u-inline-flex">
            {this.state.menu.map((m, index) => {
              return <div key={index} onClick={() => this.switchMenu(index)} className={"timeline-tab" + (this.props.activeMenu === index ? " is-active" : "")}>{t(m)}</div>
            })}
          </div>
        </div>}

        {/* LANGUAGE OPTIONS DESKTOP */}
        <div className="ml-auto u-hidden-down@tablet">
          <Button className={"btn btn-ghost-info rounded-0 px-2 btn-lang" + (this.state.activeLang === 'en' ? " disabled" : "")} onClick={() => this.saveLanguage('en')}>ENG</Button>
          <Button className={"btn btn-ghost-info rounded-0 px-3 border-left btn-lang" + (this.state.activeLang === 'id' ? " disabled" : "")} onClick={() => this.saveLanguage('id')}>ID</Button>
        </div>

        {/* DROPDOWN OPTIONS DESKTOP */}
        <Nav className="u-hidden-down@tablet" navbar>
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <div className="timeline-tab-parent u-inline-flex">
                <div className="timeline-tab-static">
                  <div><i className="fa fa-caret-down"></i></div>
                </div>
              </div>
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem header tag="div" className="text-center"><strong>{t('Setting')}</strong></DropdownItem>
              <DropdownItem onClick={this.logout}><i className="fa fa-lock"></i> {t('Logout')}</DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>

        {/* TOP MENU TABLET */}
        {defaultConfig.enableTopMenu && <Nav className="ml-auto u-hidden-up@tablet" navbar>
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <div className="timeline-tab-parent u-inline-flex">
                <div className="timeline-tab is-active u-inline-flex">
                  <div style={{ whiteSpace: 'nowrap', overflow: "hidden", width: "50px", textOverflow: "ellipsis" }}>{t(this.state.menu[this.props.activeMenu])}</div>
                  <div><i className="fa fa-caret-down"></i></div>
                </div>
              </div>
            </DropdownToggle>
            <DropdownMenu right>
              {this.state.menu.map((m, index) => {
                return <DropdownItem key={index} onClick={() => this.switchMenu(index)}>{t(m)}</DropdownItem>
              })}
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>}

        {/* DROPDOWN OPTIONS TABLET */}
        <Nav className="u-hidden-up@tablet" navbar>
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <div className="timeline-tab-parent u-inline-flex">
                <div className="timeline-tab-static">
                  <div><i className="fa fa-caret-down"></i></div>
                </div>
              </div>
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem header tag="div" className="text-center"><strong>{t('Language')}</strong></DropdownItem>
              <DropdownItem onClick={() => this.saveLanguage('en')}><i className="fa fa-flag"></i>{t('English')}</DropdownItem>
              <DropdownItem onClick={() => this.saveLanguage('id')}><i className="fa fa-flag"></i>{t('Indonesian')}</DropdownItem>
              <DropdownItem header tag="div" className="text-center"><strong>{t('Setting')}</strong></DropdownItem>
              <DropdownItem onClick={this.logout}><i className="fa fa-lock"></i> {t('Logout')}</DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>

      </React.Fragment>
    );
  }
}
function mapStateToProps(state) {

  return {
    user: state.authentication.user,
    activeMenu: state.authentication.activeMenu
  };
}

export default connect(mapStateToProps)(DefaultHeader);
