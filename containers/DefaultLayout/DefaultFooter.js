import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { t } from '../../helpers/Translate';

const propTypes = { children: PropTypes.node };

class DefaultFooter extends Component {
  render() {
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>

      </React.Fragment>
    );
  }
}

DefaultFooter.propTypes = propTypes;

function mapStateToProps(state) {

  return {
    user: state.authentication.user,
  };
}

export default connect(mapStateToProps)(DefaultFooter);


