// NOTE:
// - Code will be used for path/url routing
// - Name will be used for menu name in web

export default [
    {
        name: 'C&S',
        code: 'AC0000000',
        children: [
            {
                name: 'Confirmation',
                code: 'AC0100000',
                children: [
                    {
                        name: 'Correction',
                        code: 'AC0101000'
                    }
                ]
            }
        ]
    },
    {
        name: 'Admin',
        code: 'AZ0000000',
        children: [
            {
                name: 'User',
                code: 'AZ0100000',
                children: [
                    {
                        name: 'User Management',
                        code: 'AZ0101000',
                        children: [
                            {
                                name: 'User Management',
                                code: 'AZ0101001'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        name: 'SFA',
        hide: true
    },
    {
        name: 'Merchant',
        hide: true
    },
    {
        name: 'MMP',
        hide: true
    },
    {
        name: 'TMS',
        hide: true
    },
    {
        name: 'WDS',
        hide: true
    },
    {
        name: 'Authorization',
        hide: true
    },
    {
        name: 'Metering',
        hide: true
    },
    {
        name: 'External',
        hide: true
    },
    {
        name: 'Edu & Test',
        hide: true
    }
];