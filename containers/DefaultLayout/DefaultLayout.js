import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import * as router from 'react-router-dom';
import { Container } from 'reactstrap';
import { AppAside, AppFooter, AppHeader, AppSidebar, AppSidebarFooter, AppSidebarForm, AppSidebarHeader, AppSidebarMinimizer, AppBreadcrumb2 as AppBreadcrumb, AppSidebarNav2 as AppSidebarNav, } from '@coreui/react';

import screenList from './_screenList';
import screenRoutes from './_screenRoutes';
import defaultConfig from './_defaultConfig';
import { t } from '../../helpers/Translate';

const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));
const PrivateMenuRoute = ({ path, exact, name, component: Component, config }) => (
  <Route render={props => {
    //TO DO CHECK PERMISSION
    return <Component path={path} exact={exact} name={name} config={config} {...props} />
    // return <Redirect to={{ pathname: '/' }} />
  }} />
)
const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

class DefaultLayout extends Component {

  constructor(props) {
    super(props);

    this.state = {
      navigation: [],
      mergedNavigation: {
        items: []
      }
    };
  }

  componentDidMount() {
    let navigation = [];
    let mergedNavigation = {
      items: []
    };
    //TO DO CHECK PERMISSION 
    screenList.map(item => {
      if (!item.hide) {
        let nav = [
          {
            title: true,
            name: t(item.name),
          }
        ]
        if (item.children && item.children.length > 0) {
          item.children.map(itemTopChild => {
            let topLevel = {
              name: t(itemTopChild.name)
            };

            if (itemTopChild.children && itemTopChild.children.length > 0) {
              topLevel.icon = "icon-plus";
              let midLevels = [];
              itemTopChild.children.map(itemMidChild => {
                let midLevel = {
                  name: t(itemMidChild.name)
                };
                if (itemMidChild.children && itemMidChild.children.length > 0) {
                  midLevel.icon = "icon-plus";
                  let lowLevels = [];
                  itemMidChild.children.map(itemLowChild => {
                    let lowLevel = {
                      name: t(itemLowChild.name),
                      icon: 'fa fa-circle',
                      url: "/" + itemLowChild.code
                    };
                    lowLevels.push(lowLevel);
                  });
                  midLevel.children = lowLevels;
                }
                else {
                  midLevel.icon = 'fa fa-circle';
                  midLevel.url = "/" + itemMidChild.code;
                }
                midLevels.push(midLevel);
              });
              topLevel.children = midLevels;
            }
            else {
              topLevel.icon = 'fa fa-circle';
              topLevel.url = "/" + itemTopChild.code;
            }

            nav.push(topLevel);

          })
        }
        navigation.push({
          items: nav
        });
        mergedNavigation.items = [...mergedNavigation.items, ...nav];
      }
    })
    console.log('navigation', navigation);
    console.log('merged navigation', mergedNavigation);
    this.setState({ navigation: navigation, mergedNavigation, mergedNavigation });
  }


  render() {

    let nav = this.state.mergedNavigation;
    if (defaultConfig.enableTopMenu) {
      nav = this.state.navigation[this.props.activeMenu];
    }

    console.log('layout props', this.props)
    return (
      <div className="app">
        <AppHeader className="text-white bg-primary border-0" fixed>
          <Suspense fallback={loading()}>
            <DefaultHeader />
          </Suspense>
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
              <AppSidebarNav navConfig={nav} location={this.props.location} router={router} />
            </Suspense>
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <AppBreadcrumb appRoutes={screenRoutes} router={router} />
            <Container fluid>
              <Suspense fallback={loading()}>
                <Switch>
                  {screenRoutes.map((route, idx) => {
                    return route.component ? (
                      <PrivateMenuRoute
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        component={route.component}
                        config={route.config} />
                    ) : (null);
                  })}
                  <Redirect from="/" to="/home" />
                </Switch>
              </Suspense>
            </Container>
          </main>
        </div>
        {/* <AppFooter>
          <Suspense fallback={loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter> */}
      </div>
    );
  }
}

function mapStateToProps(state) {

  return {
    user: state.authentication.user,
    activeMenu: state.authentication.activeMenu
  };
}

export default connect(mapStateToProps)(DefaultLayout);

