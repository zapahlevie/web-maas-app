import React from 'react';
import { connect } from 'react-redux';
import { Card, CardBody, CardGroup, Col, Container, Row, Button, InputGroup, InputGroupAddon, InputGroupText, Form, Input, FormFeedback } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import ReCAPTCHA from "react-google-recaptcha";

import { authenticationActions } from '../actions/authentication.actions';
import { t } from '../helpers/Translate';
import { validate } from '../helpers/Validator';

class Login extends React.PureComponent {
    constructor(props) {
        super(props);
        if (localStorage.getItem('user')) {
            props.history.push('/');
        }
        this.state = {
            user_id: '',
            password: '',
            isError: '',
            errorMsg: '',
            isLoading: false,
            isSubmitted: false,
            isCaptchaUnchecked: false,
            width: window.innerWidth
        };
        this.onChange = this.onChange.bind(this);
        this.recaptchaRef = React.createRef();
        this.handleChange = this.handleChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this);

    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };

    componentDidMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);
    }

    onChange(value) {
        if (value) {
            this.setState({ isCaptchaUnchecked: false });
        }
    }

    handleChange(key, value) {
        this.setState({ [key]: value });
    }

    handleLogin(e) {
        e.preventDefault();
        const { dispatch } = this.props;

        this.setState({ isSubmitted: true, isError: false, errorMsg: '' });

        if (validate(this.state.user_id, 'required') && validate(this.state.password, 'required')) {
            this.setState({ isLoading: true });
            dispatch(authenticationActions.login(
                { user_id: this.state.user_id, password: this.state.password }
            )).then(() => {
                if (this.props.loginError) {
                    this.setState({ isError: true, errorMsg: this.props.loginError, isLoading: false });
                }
            })
        }
    }

    render() {

        let isMobile = this.state.width <= 600;

        let formClass = "pt-4 pl-5 pr-5 pb-4";
        if (isMobile) {
            formClass = "p-4";
        }

        if (this.props.user) {
            return <Redirect to='/' />
        }

        return (
            <div className="app flex-row align-items-center">
                <Container>
                    <Row className="justify-content-center">
                        <Col md="5" sm="12">
                            <CardGroup>
                                <Card className="p-0">
                                    <CardBody className="p-0">
                                        <Form>
                                            <div className={"l-header bg-primary " + formClass}>
                                                <div>MTI</div>
                                                <div>Acquiring</div>
                                                <div>Aggregator</div>
                                                <div>System</div>
                                            </div>
                                            {this.state.isError &&
                                                <div className="alert alert-danger">{t('Invalid username or password')}</div>
                                            }
                                            <div className={formClass}>
                                                <InputGroup className="mb-3">
                                                    <InputGroupAddon addonType="prepend">
                                                        <InputGroupText>
                                                            <i className="icon-user"></i>
                                                        </InputGroupText>
                                                    </InputGroupAddon>
                                                    <Input invalid={this.state.isSubmitted && !validate(this.state.user_id, 'required')} name="user_id" type="text" className='form-control' onChange={(e) => this.handleChange('user_id', e.target.value)} />
                                                    <FormFeedback>{t('This field is required')}</FormFeedback>
                                                </InputGroup>
                                                <InputGroup className="mb-4">
                                                    <InputGroupAddon addonType="prepend">
                                                        <InputGroupText>
                                                            <i className="icon-lock"></i>
                                                        </InputGroupText>
                                                    </InputGroupAddon>
                                                    <Input invalid={this.state.isSubmitted && !validate(this.state.password, 'required')} autoComplete="off" name="password" type="password" className='form-control' onChange={(e) => this.handleChange('password', e.target.value)} />
                                                    <FormFeedback>{t('This field is required')}</FormFeedback>
                                                </InputGroup>
                                                <ReCAPTCHA
                                                    ref={this.recaptchaRef}
                                                    size="invisible"
                                                    sitekey="6LcdyK8ZAAAAAJwpc4EKeb2g14ckvrEUYiSYqFvc"
                                                    onChange={this.onChange}
                                                />
                                                {this.state.isCaptchaUnchecked && <div>
                                                    <small className="text-danger">{t('Please complete captcha above')}</small>
                                                </div>}
                                                <Row className="mt-4">
                                                    <Col xs="6">
                                                        <Button onClick={(e) => this.handleLogin(e)} type="submit" color="primary" className="px-4" disabled={this.state.isLoading}>{t('Login')}</Button>
                                                    </Col>
                                                    <Col xs="6" className="text-right">
                                                        <Link to={'/login'} className="px-0">{t('Forgot password?')}</Link>
                                                    </Col>
                                                </Row>
                                            </div>
                                        </Form>
                                    </CardBody>
                                </Card>
                            </CardGroup>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

function mapStateToProps(state) {

    return {
        user: state.authentication.user,
        loginError: state.authentication.loginError
    };
}

export default connect(mapStateToProps)(Login);