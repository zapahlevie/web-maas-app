import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { screen } from './screenreducer';

const rootReducer = combineReducers({
    authentication,
    screen
});

export default rootReducer;