import { screenConstants } from '../actions/screen.actions'

const initialState = {
  getAllResult: [],
  getAllTotal: 0,
  error: '',
};

export function screen(state = initialState, action) {
  switch (action.type) {
    case screenConstants.SCREEN_GET_ALL_SUCCESS:
      return Object.assign({}, state, {
        getAllResult: action.data.data,
        getAllTotal: action.data.total,
        error: '',
      });
    case screenConstants.SCREEN_GET_ALL_ERROR:
      return Object.assign({}, state, {
        getAllResult: [],
        getAllTotal: 0,
        error: action.errorMsg
      });
    case screenConstants.SCREEN_CREATE_SUCCESS:
      return Object.assign({}, state, {
        error: '',
      });
    case screenConstants.SCREEN_CREATE_ERROR:
      return Object.assign({}, state, {
        error: action.errorMsg
      });
    case screenConstants.SCREEN_UPDATE_SUCCESS:
      return Object.assign({}, state, {
        error: '',
      });
    case screenConstants.SCREEN_UPDATE_ERROR:
      return Object.assign({}, state, {
        error: action.errorMsg
      });
    case screenConstants.SCREEN_DELETE_SUCCESS:
      return Object.assign({}, state, {
        error: '',
      });
    case screenConstants.SCREEN_DELETE_ERROR:
      return Object.assign({}, state, {
        error: action.errorMsg
      });
    case screenConstants.SCREEN_RESET_STATE:
      return Object.assign({}, state, {
        getAllResult: [],
        getAllTotal: 0,
        error: '',
      });
    default:
      return state
  }
}