import { authenticationConstants } from '../actions/authentication.actions'

const initialState = {
  loggedIn: false,
  activeMenu: 0,
  user: null,
  loginError: false
};

export function authentication(state = initialState, action) {
  switch (action.type) {
    case authenticationConstants.USER_LOGIN_SUCCESS:
      return Object.assign({}, state, {
        loggedIn: true,
        user: action.data,
        loginError: false
      });
    case authenticationConstants.USER_LOGIN_FAILURE:
    case authenticationConstants.USER_LOGOUT:
      return Object.assign({}, state, {
        loggedIn: false,
        user: null,
        loginError: true
      });
    case authenticationConstants.USER_CHANGE_ACTIVE_MENU:
      return Object.assign({}, state, {
        activeMenu: action.menu
      });
    default:
      return state
  }
}