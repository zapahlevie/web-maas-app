import React, { Component } from 'react';
import { Button, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';

import { t } from '../helpers/Translate';

class ModalConfirm extends Component {

    render() {
        const { header, body } = this.props;
        return (
            <Modal isOpen toggle={this.props.toggle} className='modal-primary-sm'>
                <ModalHeader toggle={this.props.toggle}>{t(header)}</ModalHeader>
                <ModalBody>
                    <span>{t(body)}</span>
                </ModalBody>
                <ModalFooter>
                    <Button className="px-4" color="primary" onClick={this.props.confirm}>{t('Ok')}</Button>
                    <Button className="px-4" color="light" onClick={this.props.toggle}>{t('Cancel')}</Button>
                </ModalFooter>
            </Modal>
        );
    }
}

export default ModalConfirm;
