import React, { Component } from 'react';
import { Button, Modal, ModalBody, ModalHeader, ModalFooter, FormGroup, Col, Label, Input, FormFeedback } from 'reactstrap';

import { t } from '../helpers/Translate';
import { validate } from '../helpers/Validator';

class ModalForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            fieldValues: {},
            fieldValidation: {},
            fieldValidationError: {},
        }
        this.handleSearchValueChange = this.handleSearchValueChange.bind(this);
        this.handleConfirm = this.handleConfirm.bind(this);
    }

    componentDidMount() {
        const { fields } = this.props;
        
        let fieldValues = {};
        let fieldValidation = {};
        let fieldValidationError = {};

        fields && fields.map(field => {

            //search field values
            if (field.type === 'group') {
                fieldValues[field.name] = field.fields[0] ? field.fields[0].name : '';
                field.fields.map(groupField => {
                    fieldValues[groupField.name] = groupField.defaultValue ? groupField.defaultValue : '';
                })
            }
            else if (field.type === 'date-range') {
                fieldValues[field.fields[0].name] = field.fields[0].defaultValue ? field.fields[0].defaultValue : '';
                fieldValues[field.fields[1].name] = field.fields[1].defaultValue ? field.fields[1].defaultValue : '';
            }
            else if (field.type === 'checkbox') {
                fieldValues[field.name] = (field.defaultValue && field.defaultValue.length > 0) ? [...field.defaultValue] : [];
            }
            else {
                fieldValues[field.name] = field.defaultValue ? field.defaultValue : '';
            }

            //search validation
            if (field.type === 'group') {
                field.fields.map(groupField => {
                    if (groupField.validation && groupField.validation.length > 0) {
                        fieldValidation[groupField.name] = groupField.validation;
                        fieldValidationError[groupField.name] = false;
                    }
                });
            }
            else if (field.type === 'date-range') {
                fieldValidation[field.fields[0].name] = field.fields[0].validation;
                fieldValidation[field.fields[1].name] = field.fields[0].validation;
                fieldValidationError[field.fields[0].name] = false;
                fieldValidationError[field.fields[0].name] = false;
            }
            else {
                if (field.validation && field.validation.length > 0) {
                    fieldValidation[field.name] = field.validation;
                    fieldValidationError[field.name] = false;
                }
            }

        })

        this.setState({ fieldValues, fieldValidation, fieldValidationError });

    }

    handleSearchValueChange(key, value) {

        if (Array.isArray(this.state.fieldValues[key])) {
            let arrValues = [...this.state.fieldValues[key]];
            if (arrValues.includes(value)) {
                arrValues = arrValues.filter(val => { return val != value; })
            }
            else {
                arrValues.push(value);
            }
            this.setState({ fieldValues: Object.assign({}, this.state.fieldValues, { [key]: arrValues }) });
        }
        else {
            this.setState({ fieldValues: Object.assign({}, this.state.fieldValues, { [key]: value }) });
        }

    }

    printValidation(validators) {
        if (Array.isArray(validators)) {
            let arr = [];
            validators.map((value, index) => {
                if (value.indexOf('minlength-') === 0) {
                    arr.push(t('at least [n] characters', { n: value.split('-')[1] }));
                }
                else if (value.indexOf('maxlength-') === 0) {
                    arr.push(t('[n] characters maximum', { n: value.split('-')[1] }));
                }
                else {
                    arr.push(t(value));
                }
            })
            return arr.toString().split(',').join(', ');
        }
        else {
            return '';
        }
    }

    isFieldsValidationPassed() {
        //Clear Error
        let fieldValidationError = {};
        Object.keys(this.state.fieldValidationError).map(key => {
            fieldValidationError[key] = false;
        })
        this.setState({ fieldValidationError });

        //do validate
        Object.keys(this.state.fieldValidation).map(key => {

            let value = this.state.fieldValues[key];
            let validators = this.state.fieldValidation[key];

            if (!validate(value, validators)) {
                fieldValidationError[key] = true;
            }

        })

        //check if error exist
        let isValidationPassed = true;
        Object.keys(this.state.fieldValidationError).map(key => {
            if (fieldValidationError[key]) {
                isValidationPassed = false;
            }
        })

        return isValidationPassed;
    }

    handleConfirm() {
        if (this.isFieldsValidationPassed()) {
            this.props.confirm(this.state.fieldValues)
        }
    }

    render() {
        const { header, fields } = this.props;
        const { fieldValidationError, fieldValues, fieldValidation } = this.state;

        return (
            <Modal isOpen toggle={this.props.toggle} className='modal-primary-sm'>
                <ModalHeader toggle={this.props.toggle}>{t(header)}</ModalHeader>
                <ModalBody>
                    {fields && fields.map((field, index) => {

                        if (field.type === 'date-range') {
                            return (
                                <FormGroup row key={index}>
                                    <Col xs="3" sm="3" md="3">
                                        <Label htmlFor={field.fields[0].name}>{t(field.label)}</Label>
                                    </Col>
                                    <Col xs="4" sm="4" md="4">
                                        <Input invalid={fieldValidationError[field.fields[0].name]} onChange={(e) => this.handleSearchValueChange(field.fields[0].name, e.target.value)} name={field.fields[0].name} type="date" value={fieldValues[field.fields[0].name] || ''} />
                                        <FormFeedback>{t('This field is')} {this.printValidation(fieldValidation[field.name])}</FormFeedback>
                                    </Col>
                                    <Col xs="1" sm="1" md="1" className="text-center">
                                        <Label htmlFor={field.fields[1].name}>-</Label>
                                    </Col>
                                    <Col xs="4" sm="4" md="4">
                                        <Input invalid={fieldValidationError[field.fields[1].name]} onChange={(e) => this.handleSearchValueChange(field.fields[1].name, e.target.value)} name={field.fields[1].name} type="date" value={fieldValues[field.fields[1].name] || ''} />
                                        <FormFeedback>{t('This field is')} {this.printValidation(fieldValidation[field.name])}</FormFeedback>
                                    </Col>
                                </FormGroup>
                            )
                        }
                        else if (field.type === 'date') {
                            return (
                                <FormGroup row key={index}>
                                    <Col xs="3" sm="3" md="3">
                                        <Label htmlFor={field.name}>{t(field.label)}</Label>
                                    </Col>
                                    <Col xs="9" sm="9" md="9">
                                        <Input invalid={fieldValidationError[field.name]} onChange={(e) => this.handleSearchValueChange(field.name, e.target.value)} name={field.name} type="date" value={fieldValues[field.name] || ''} />
                                        <FormFeedback>{t('This field is')} {this.printValidation(fieldValidation[field.name])}</FormFeedback>
                                    </Col>
                                </FormGroup>
                            )
                        }
                        else if (field.type === 'group') {
                            return (
                                <FormGroup row key={index}>
                                    <Col xs="3" sm="3" md="3">
                                        <Input onChange={(e) => this.handleSearchValueChange(field.name, e.target.value)} type="select" name={field.name} value={fieldValues[field.name] || ''}>
                                            {field.fields.map((groupField, index) => {
                                                return <option key={index} value={groupField.name}>{t(groupField.label)}</option>
                                            })}
                                        </Input>
                                    </Col>
                                    <Col xs="9" sm="9" md="9">
                                        {field.fields.map((groupField, index) => {
                                            let activeGroupField = fieldValues[field.name] || '';
                                            let isActive = false;
                                            if (activeGroupField) {
                                                isActive = activeGroupField === groupField.name;
                                            }
                                            else {
                                                if (index === 0) {
                                                    isActive = true;
                                                }
                                            }
                                            return (
                                                <div key={index} className={isActive ? '' : 'd-none'}>
                                                    <Input invalid={fieldValidationError[groupField.name]} onChange={(e) => this.handleSearchValueChange(groupField.name, e.target.value)} name={groupField.name} type={groupField.type ? groupField.type : 'text'} value={fieldValues[groupField.name] || ''} />
                                                    <FormFeedback>{t('This field is')} {this.printValidation(fieldValidation[groupField.name])}</FormFeedback>
                                                </div>
                                            )
                                        })}
                                    </Col>
                                </FormGroup>
                            )
                        }
                        else if (field.type === 'select') {
                            return (
                                <FormGroup row key={index}>
                                    <Col xs="3" sm="3" md="3">
                                        <Label htmlFor={field.name}>{t(field.label)}</Label>
                                    </Col>
                                    <Col xs="9" sm="9" md="9">
                                        <Input invalid={fieldValidationError[field.name]} onChange={(e) => this.handleSearchValueChange(field.name, e.target.value)} name={field.name} type="select" value={fieldValues[field.name] || ''} >
                                            <option value="">{t('Please select')}...</option>
                                            {field.options.map((option, index) => {
                                                return <option key={index} value={option.value}>{option.label}</option>
                                            })}
                                        </Input>
                                        <FormFeedback>{t('This field is')} {this.printValidation(fieldValidation[field.name])}</FormFeedback>
                                    </Col>
                                </FormGroup>
                            )
                        }
                        else if (field.type === 'radio') {
                            return (
                                <FormGroup row key={index}>
                                    <Col xs="3" sm="3" md="3">
                                        <Label htmlFor={field.name}>{t(field.label)}</Label>
                                    </Col>
                                    <Col xs="9" sm="9" md="9">
                                        {field.options.map((option, index) => {
                                            let isChecked = fieldValues[field.name] && (fieldValues[field.name] === option.value);
                                            return (
                                                <Label key={index} htmlFor={field.name} className="ml-4 mr-2">
                                                    <Input invalid={fieldValidationError[field.name]} onChange={(e) => this.handleSearchValueChange(field.name, e.target.value)} name={field.name} type="radio" value={option.value} checked={isChecked ? true : false} />
                                                    {option.label}
                                                    <FormFeedback>{t('This field is')} {this.printValidation(fieldValidation[field.name])}</FormFeedback>
                                                </Label>
                                            )
                                        })}
                                    </Col>
                                </FormGroup>
                            )
                        }
                        else if (field.type === 'checkbox') {
                            return (
                                <FormGroup row key={index}>
                                    <Col xs="3" sm="3" md="3">
                                        <Label htmlFor={field.name}>{t(field.label)}</Label>
                                    </Col>
                                    <Col xs="9" sm="9" md="9">
                                        {field.options.map((option, index) => {
                                            let isChecked = fieldValues[field.name] && (fieldValues[field.name].length > 0) && fieldValues[field.name].includes(option.value);
                                            return (
                                                <Label key={index} htmlFor={field.name} className="ml-4 mr-2">
                                                    <Input invalid={fieldValidationError[field.name]} onChange={(e) => this.handleSearchValueChange(field.name, e.target.value)} name={field.name} type="checkbox" value={option.value} checked={isChecked ? true : false} />
                                                    {option.label}
                                                    <FormFeedback>{t('This field is')} {this.printValidation(fieldValidation[field.name])}</FormFeedback>
                                                </Label>
                                            )
                                        })}
                                    </Col>
                                </FormGroup>
                            )
                        }
                        else {
                            return (
                                <FormGroup row key={index}>
                                    <Col xs="3" sm="3" md="3">
                                        <Label htmlFor={field.name}>{t(field.label)}</Label>
                                    </Col>
                                    <Col xs="9" sm="9" md="9">
                                        <Input invalid={fieldValidationError[field.name]} onChange={(e) => this.handleSearchValueChange(field.name, e.target.value)} name={field.name} type={field.type ? field.type : 'text'} value={fieldValues[field.name] || ''} />
                                        <FormFeedback>{t('This field is')} {this.printValidation(fieldValidation[field.name])}</FormFeedback>
                                    </Col>
                                </FormGroup>
                            )
                        }
                    })}
                </ModalBody>
                <ModalFooter>
                    <Button className="px-4" color="primary" onClick={this.handleConfirm}>{t('Save')}</Button>
                    <Button className="px-4" color="light" onClick={this.props.toggle}>{t('Cancel')}</Button>
                </ModalFooter>
            </Modal>
        );
    }
}

export default ModalForm;
