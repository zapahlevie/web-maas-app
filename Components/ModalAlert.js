import React, { Component } from 'react';
import { Button, Modal, ModalBody } from 'reactstrap';

import { t } from '../helpers/Translate';

class ModalAlert extends Component {

    render() {
        const { handler, confirm, className, message } = this.props;
        return (
            <Modal isOpen toggle={handler}
                className={'modal-primary-sm' + className}>
                <ModalBody>
                    <div>
                        <span>{message}</span>
                    </div>
                    <div className="mt-3">
                        <Button className="pull-right px-4" color="primary" onClick={confirm}>{t('Ok')}</Button>
                    </div>
                </ModalBody>
            </Modal>
        );
    }
}

export default ModalAlert;
