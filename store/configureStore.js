import { createStore, applyMiddleware } from "redux";
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import thunkMiddleware from "redux-thunk";

import rootReducer from "../reducers/index";
import promiseMiddleware from "../middleware/promiseMiddleware";

const persistConfig = {
    key: 'root',
    storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export default () => {
    let store = createStore(persistedReducer, applyMiddleware(
        thunkMiddleware,
        promiseMiddleware
    ))
    let persistor = persistStore(store)
    return { store, persistor }
}